﻿using Microsoft.EntityFrameworkCore;
using Products.Sheard.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Products.DAO
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options) : base(options)
        {

        }

        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Price> Price { get; set; }

        public override void Dispose()
        {
            base.Dispose();
        }

    }
}
