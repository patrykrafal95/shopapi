﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Products.Sheard.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public virtual Price Price { get; set; }
    }
}
