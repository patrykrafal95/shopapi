﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Products.Sheard.Models
{
    public class Price
    {
        public int ID { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Value { get; set; }
        public string Supplier { get; set; }

        public Product Product { get; set; }
        public int ProductId { get; set; }
    }
}
