﻿
using Microsoft.EntityFrameworkCore;
using Products.DAO;
using Products.Sheard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Products.Sheard.Services
{
    public interface ISeedData
    {
        void SaveSampleData();
    }
    public class SeedData : ISeedData
    {
        private readonly ProductContext _productContext;
        public SeedData(ProductContext productContext) => _productContext = productContext;
        public void SaveSampleData()
        {
            this._productContext.Database.Migrate();

            if (!this._productContext.Product.Any())
            {
                this._productContext.AddRange(
                     new Product
                     {
                         Name = "Wiedźmin 4"
                     },
                 new Product
                 {
                     Name = "Gothic 3"
                 });
            }
            this._productContext.SaveChanges();

            if (!this._productContext.Price.Any())
            {
                this._productContext.AddRange(
                     new Price
                     {
                         ProductId = 1,
                         Supplier = "Wiedźmin Polska",
                         Value = 250
                     },
                 new Price
                 {
                     ProductId = 2,
                     Supplier = "Gothic Polska",
                     Value = 250
                 });
            }
            this._productContext.SaveChanges();


        }
    }
}
