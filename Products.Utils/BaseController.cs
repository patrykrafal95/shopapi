﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Products.Utils
{
    public abstract class BaseController : Controller
    {
        protected readonly Logger _logger =  null;


        public BaseController()
        {
            this._logger = LogManager.GetLogger(this.GetType().Name);
            this._logger.Info(this);
        }
    }
}
