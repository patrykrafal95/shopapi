﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Products.Sheard.Models;
using Products.Sheard.Services;
using Products.Utils;

namespace Products.Web.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : BaseController
    {
        private readonly IProductService _productService;
        private readonly ISeedData _seedData;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productService"></param>
        #region
        public ProductController(IProductService productService,ISeedData seedData)
        {
            _productService = productService;
            _seedData = seedData;
        }
        #endregion

        /// <summary>
        /// Get products
        /// </summary>
        /// <returns></returns>
        #region
        [HttpGet]
        public async Task<IActionResult> GetProducts()
        {
            this._seedData.SaveSampleData();
            IEnumerable<object> products = await this._productService.GetAllProducts();
            if (products != null)
            {
                return new OkObjectResult(products);
            }
            return new NotFoundResult();
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        #region
        [HttpGet, Route("product/{id}")]
        public async Task<IActionResult> GetProductId(int? id)
        {
            if (id != null)
            {
                var product = await this._productService.GetProduct(id);
                return new OkObjectResult(product);
            }
            return new NotFoundResult();

        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        #region
        [HttpGet, Route("findProduct")]
        public async Task<IActionResult> GetProductByName(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                var product = await this._productService.FindProductByName(name);
                return new OkObjectResult(product);
            }
            return new NotFoundResult();
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        #region
        [HttpPost, Route("saveProduct")]
        public async Task<IActionResult> SaveProduct(Product p)
        {
            return Ok(await this._productService.SaveProduct(p));
        }
        #endregion

        [HttpGet,Route("remove/{id}")]
        public async Task<IActionResult>RemoveProduct(int id)
        {
            return Ok(await this._productService.DeleteProduct(id));
        }


    }
}
