﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Products.DAO;
using Products.Sheard.Models;

namespace Products.Sheard.Services
{
    public class ProductService : IProductService
    {
        private readonly ProductContext _context;

        public ProductService(ProductContext context) => _context = context;

        public async Task<Product> DeleteProduct(int productId)
        {
            Product p = await GetProduct(productId);
            if (p != null)
            {
                this._context.Remove(p);
                await _context.SaveChangesAsync();
            }
            return p;
        }

        public async Task<IEnumerable<object>> FindProductByName(string name)
        {
            var productQuery = this._context.Product.AsQueryable();

            var product = await productQuery.Where(
                p => p.Name.Contains(name, StringComparison.OrdinalIgnoreCase)).
                Select(_ => new
                {
                    _.ID,
                    _.Name,
                    p = new
                    {
                        _.Price.Supplier,
                        _.Price.Value
                    }
                }).ToListAsync();

            return product;
        }
            

        public async Task<IEnumerable<object>> GetAllProducts()
        {
            var productsQuery = this._context.Product.AsQueryable();
            var products = await productsQuery.Select(_ =>
            new
            {
                _.ID,
                _.Name,
                price = _.Price != null ? new
                {
                   _.Price.Supplier,
                   _.Price.Value
                } : null
            }).ToListAsync();

            return products;
        }

        public async Task<Product> GetProduct(int? productId) =>
            await this._context.Product.Where(p => p.ID == productId).FirstOrDefaultAsync();


        public async Task<Product> SaveProduct(Product p)
        {
            if(p != null)
            {
                this._context.Add(p);
                await this._context.SaveChangesAsync();
            }
            return p;
        }
    }
}
