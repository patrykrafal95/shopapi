﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Products.DAO;
using Products.Sheard.Models;

namespace Products.Sheard.Services
{
    public class PriceServie : IPriceService
    {

        private readonly ProductContext _context;
        
        public PriceServie(ProductContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Price>> GetPricesAsync(int priceId)
        {
            return await _context.Price.Where(_ => _.ID == priceId).ToListAsync();
        }
    }
}
