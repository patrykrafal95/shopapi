﻿using Products.Sheard.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Products.Sheard.Services
{
    public interface IProductService
    {
        Task<Product> SaveProduct(Product p);
        Task<IEnumerable<object>> GetAllProducts();
        Task<Product> GetProduct(int? productId);
        Task<IEnumerable<object>> FindProductByName(string name);
        Task<Product> DeleteProduct(int productId);

    }
}
