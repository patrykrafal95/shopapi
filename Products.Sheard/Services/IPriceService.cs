﻿using Products.Sheard.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Products.Sheard.Services
{
    public interface IPriceService
    {
        Task <IEnumerable<Price>> GetPricesAsync(int priceId);
    }
}
